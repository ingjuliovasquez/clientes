@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="mb-3">Registro de Clientes</h3>

    <div class="card p-5">
        <form action="/clientes" method="POST">
            @csrf
            <div class="row">
                <div class="col-6">
                    <label>Cedula*</label>
                    <input type="number" name="cedula" class="form-control">
                    @error('cedula')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-6">
                    <label>Nombre*</label>
                    <input type="text" name="nombres" value="" class="form-control">
                    @error('nombres')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <label>Apellidos*</label>
                    <input type="text" name="apellidos" class="form-control">
                    @error('apellidos')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-6">
                    <label>Celular*</label>
                    <input type="text" name="celular" value="" class="form-control">
                    @error('celular')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <label>Correo*</label>
                    <input type="email" name="correo" class="form-control">
                    @error('correo')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-6">
                    <label>direccion*</label>
                    <input type="text" name="direccion" value="" class="form-control">
                    @error('direccion')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="text-right mt-5">
                <a href="/clientes" class="btn btn-danger">Cancelar</a>
                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
        </form>
    </div>
</div>
@endsection