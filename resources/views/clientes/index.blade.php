@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-between align-items-end mb-3">
        <h3 class="m-0">Clientes</h3>
        
     
    </div>
    <a href="/token" class="btn btn-warning">WS Token</a>
    <a href="/listado" class="btn btn-success">WS Clients</a>
    <a href="/clientes/create" class="btn btn-primary">Agregar Cliente</a>
    <div class="card p-5">
        <table class="table table-striped table-hover">
            <thead>
                <th>nombre</th>
                <th>apellidos</th>
                <th>Cedula</th>
                <th>Correo</th>
                <th class="text-center">Opciones</th>
            </thead>
            <tbody>
                @foreach($items as $index => $item)
                <tr>
                    <td>{{ $item->nombres }}</td>
                    <td>{{ $item->apellidos }}</td>
                    <td>{{ $item->cedula }}</td>
                    <td>${{ $item->correo }}</td>
                    <td class="text-center d-flex alingn-items-center justify-content-center">
                        <a href="/clientes/{{ $item->id }}/edit" class="text-primary mr-1 d-flex alingn-items-center">
                            <i class="far fa-edit"></i>
                        </a>
                        <form action="/clientes/{{ $item->id }}" method="POST">
                            @csrf
                            @method('DELETE')

                            <button class="text-danger btn btn-link m-0 p-0 d-flex alingn-items-center">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection