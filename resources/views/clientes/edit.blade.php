@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="mb-3">Registro de Clientes</h3>

    <div class="card p-5">
        <form action="/edicion" method="POST">
            @csrf
            <div class="row">
                <input type="hidden" value="{{ $item->id }}" name="">
                <div class="col-6">
                    <label>Nombre*</label>
                    <input type="text" name="nombres" class="form-control" value="{{ $item->nombres }}">
                    @error('nombres')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-6">
                    <label>Cedula*</label>
                    <input type="number" name="cedula" class="form-control" value="{{ $item->cedula }}">
                    @error('cedula')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="text-right mt-5">
                <a href="/clientes" class="btn btn-danger">Cancelar</a>
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
        </form>
    </div>
</div>
@endsection