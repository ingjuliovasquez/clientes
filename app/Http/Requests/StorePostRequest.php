<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cedula' => 'required|numeric',
            'nombres' => 'required',
            'apellidos' => 'required',
            'celular' => 'required|numeric',
            'correo' => 'required',
            'direccion' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'cedula.required' => 'Por favor ingrese una cedula',
            'nombres.required' => 'Por favor seleccione un nombre',
            'apellidos.required' => 'Por favor ingrese un apellido',
            'celular.required' => 'Por favor ingrese el celular',
            'correo.required' => 'Por favor ingrese el correo',
            'direccion.required' => 'Por favor ingrese el dirección',
            'cedula.required' => 'Por favor ingrese el dirección',
            
        ];
    }
}
