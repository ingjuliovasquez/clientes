<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use App\Http\Requests\StorePostRequest;

class ClienteController extends Controller
{
    //

    public function index()
    {
        $items = Cliente::all();
        return view('clientes.index')->with(['items' => $items]);
    }

    public function create()
    {
      
        $orders = Cliente::all();

        return view('clientes.store')->with([
       
            'orders' => $orders,
        ]);
    }

    public function store(StorePostRequest $request)
    {
        
        Cliente::create($request->all());
        return redirect('/clientes');
    }


    public function edit($id)
    {
        $item = Cliente::find($id);
      
        return view('clientes.edit')->with([
            'item' => $item,

        ]);
    }

    public function cambios(StorePostRequest $request, $id)
    {

        Cliente::find($id)->fill($request->all())->save();
        return redirect('/clientes');
    }

    public function destroy($id)
    {
        Cliente::find($id)->delete();
        return back();
    }

    public function token()
    {
        $arreglo = array(
            "grant_type" => 'client_credentials'
            
        );
        $login = 'SEGUROS';
        $password = 'pruebas2021*';
        $url = 'http://3.208.158.199/magnussucre/wsdl/tokenApp';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $arreglo);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
        $result = curl_exec($ch);
        curl_close($ch);  
        return(json_decode($result, true));
    }

    public function listado()
    {
        $json = [
            "document" =>"1757612005"
           
        ];

        $url = "http://3.208.158.199/magnussucre/wsdl/customerApp";
        $content = json_encode($json);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Content-type: application/json",
            "Authorization: Bearer eyJpdiI6InQ2VlVKY2RmM3BQay9uSi84VWJDQkE9PSIsInZhbHVlIjoiYk80ZENOZ1NsTStnU2wwZlV0Uks2bUxpb2pTMG1tbkN2Zk5mME9mcWlldWg5eTFxeVlnbVRKcGhicW04eGs2USIsIm1hYyI6IjA2YjlhMWYyNGFmNDU2ZTA0MmU3YmZkZTdhNWNmMTRjYTAxOTI3MDkyNWI4NWJjM2E2ZjRhYmY3Mjc0MzMzZmUifQ=="
        ));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        $json_response = curl_exec($curl);
        // dd($json_response.mensaje);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        return(json_decode($json_response, true));
    }
    


   
}
